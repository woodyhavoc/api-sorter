use clap::Parser;
use rand::Rng;
use serde::{Deserialize, Serialize};

use std::io::{BufRead, BufReader, Write};
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;

#[derive(Parser, Debug)]
#[clap(
    author = "Aaron Woods",
    about = "A simple client for interacting with the api-sorter server."
)]
struct Args {
    #[clap(short, long)]
    /// The number of lists to generate
    count: u32,
    #[clap(short, long)]
    /// The size of each list
    size: u32,
}

#[derive(Serialize, Deserialize, Debug)]
struct UnsortedList {
    data: Vec<u32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct SortedList {
    data: Vec<u32>,
}

fn close_socket_server() {
    let mut stream = TcpStream::connect("127.0.0.1:50000").unwrap();
    let msg = "shutdown";

    stream.write_all(msg.as_bytes()).expect("failed to write");
}

fn validate_list(list: Vec<u32>) -> bool {
    let list_len = list.len();
    let mut i = 0;

    while i < (list_len - 1) {
        i += 1;
        assert!(list[i] > list[i - 1]);
    }

    true
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    let socket_server = thread::spawn(move || {
        let tcp_listener = TcpListener::bind("127.0.0.1:50000").unwrap();

        for incoming_stream in tcp_listener.incoming() {
            let mut reader = BufReader::new(incoming_stream.unwrap());
            let mut buf = String::new();

            let len = reader.read_line(&mut buf).unwrap();

            if buf == "shutdown" || len == 0 {
                break;
            }
            let sorted_list: SortedList = serde_json::from_str(&buf).unwrap();

            assert!(validate_list(sorted_list.data));
        }
    });

    let mut rng = rand::thread_rng();
    for _ in 0..args.count {
        let rand_vals = UnsortedList {
            data: (0..args.size).map(|_| rng.gen::<u32>()).collect(),
        };

        let post_resp = reqwest::Client::new()
            .post("http://127.0.0.1:40000/ingest")
            .json(&rand_vals)
            .send()
            .await?;

        let post_text = post_resp.text().await.unwrap();
        assert_eq!(post_text, "Success");
    }

    close_socket_server();
    socket_server.join().unwrap();

    Ok(())
}
