# API Sorter

The API Sorter receives lists of unsorted numbers via HTTP and then returns the sorted list via TCP.

## Goal

The goal of this project is to demonstrate the following:

- Sending data via HTTP
- Sending data via TCP
- Recursion via Merge Sort implementation
- Multithreading
- Unit Testing
- Integration Testing
- CI/CD Management

## Description

This project compiles into two programs, api-sorter and client.

### api-sorter
The `api-sorter` starts up an HTTP server and listens at endpoint `"/ingest"` for incoming unsorted lists.
Upon receiving such a list, it leverages the provided implementation of Merge Sort and sorts the list.
Finally, the `api-sorter` opens a TCP connection with the `client` and sends the sorted list.

### client
The `client` immediately starts up a TCP server to listen for sorted lists coming back from `api-sorter`.
Next the `client` will begin sending unsorted lists to `api-sorter` using HTTP POST requests.
The number of lists and the size of each list must be passed as arguments.
When the TCP server receives a sorted list, it runs a verification check to confirm that the list is sorted.
Once all the lists have been processed, the TCP server will close and the program will terminate.

## Building

To build this project run `cargo build`.

## Usage

After building, the project can be run.  To do so, `api-sorter` must start first.  After `api-sorter`
is up and running, the client can be executed similarly to this: `cargo run --bin client -- -c 10 -s 100`.
Here is the help output for further clarity:

```
$ cargo run --bin client -- --help

A simple client for interacting with the api-sorter server.

Usage: client --count <COUNT> --size <SIZE>

Options:
  -c, --count <COUNT>  The number of lists to generate
  -s, --size <SIZE>    The size of each list
  -h, --help           Print help

```

## Testing

To run the unit tests, execute the following command: `cargo test`.
To run the integration tests, execute the following command: `ruby test/integration.rb`.

The unit tests are for covering the Merge Sort implementation.
The integration tests run `api-sorter` and then call the client multiple times with different arguments for count and size.

## Areas for Improvement

There are still a number of areas for improvement.  Please note this list may not be exhaustive.

### api-sorter
- This could be enhanced to allow IP/Port combinations to be passed as command line arguments.

### client
- Enhance to allow IP/Port combinations to be passed as command line arguments.
- Enhance to allow the `client` to wait for server to be available if the `client` is launched first.

### CI/CD
- The image for the integration testing stage is not setup properly.  I believe this is leading to an issue where expected `cargo` commands are unavailable.
- It seems as though each task is compiling the project instead of using the results of the build stage.
- The repository needs semantic versioning to be handled automatically when MRs are merged into `main`.
- The repository needs code coverage reporting with a percentage badge.

### Testing
- The script `test/integration.rb` is not executing as expected.  As mentioned above, it seems as though expected `cargo` commands are not available inside the script.

## Final Thoughts

I had an amazing time working on this project.  I learned quite a bit, specifically about Rust which I had never used before.
Being able to piece together so many different moving parts has been incredibly rewarding for me.  C has for a long time
been my favorite language (for compiled languages anyway), but after this I think Rust could be a contender for top spot!
