use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::io::{Result, Write};
use std::net::TcpStream;
use std::str;

#[derive(Serialize, Deserialize, Debug)]
struct UnsortedList {
    data: Vec<u32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct SortedList {
    data: Vec<u32>,
}

fn merge(list: &mut UnsortedList, begin: usize, mid: usize, end: usize) {
    let llen = mid - begin + 1;
    let rlen = end - mid;

    let mut llist = vec![0; llen];
    let mut rlist = vec![0; rlen];

    // copy left half of unsorted list into llist
    llist[..llen].copy_from_slice(&list.data[begin..(llen + begin)]);

    // copy right half of unsorted list into rlist
    rlist[..rlen].copy_from_slice(&list.data[(mid + 1)..(rlen + mid + 1)]);

    let mut left = 0;
    let mut right = 0;
    let mut k = begin;

    loop {
        if left >= llen && right >= rlen {
            break;
        }

        if right == rlen || (left < llen && llist[left] < rlist[right]) {
            list.data[k] = llist[left];
            left += 1;
        } else {
            list.data[k] = rlist[right];
            right += 1;
        }

        k += 1;
    }
}

fn merge_sort(list: &mut UnsortedList, begin: usize, end: usize) {
    if begin < end {
        let mid = (begin + end) / 2;
        merge_sort(list, begin, mid);
        merge_sort(list, mid + 1, end);
        merge(list, begin, mid, end);
    }
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[post("/ingest")]
async fn ingest(mut req_body: web::Json<UnsortedList>) -> impl Responder {
    let mut list = UnsortedList {
        data: req_body.data.clone(),
    };

    let data_len = list.data.len() - 1;

    merge_sort(&mut list, 0, data_len);

    let mut stream = TcpStream::connect("127.0.0.1:50000").unwrap();

    let sorted_list = SortedList {
        data: list.data.clone(),
    };
    req_body.data.sort();
    let serialized = serde_json::to_string(&sorted_list).unwrap();

    stream
        .write_all(&serialized.into_bytes())
        .expect("failed to write");

    HttpResponse::Ok().body("Success")
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[actix_web::main]
async fn main() -> Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .service(ingest)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind("127.0.0.1:40000")?
    .run()
    .await
}

#[allow(dead_code)]
fn run_test(list_size: u32) -> (Vec<u32>, Vec<u32>) {
    let mut rng = rand::thread_rng();
    let mut list = UnsortedList {
        data: (0..list_size).map(|_| rng.gen::<u32>()).collect(),
    };
    let mut clone = list.data.clone();
    let begin = 0;
    let end = if list_size > 0 {
        list.data.len() - 1
    } else {
        0
    };

    merge_sort(&mut list, begin, end);
    clone.sort();

    (list.data, clone)
}

#[test]
fn test0() {
    let (list, clone) = run_test(0);
    assert_eq!(list, clone);
}

#[test]
fn test1() {
    let (list, clone) = run_test(1);
    assert_eq!(list, clone);
}

#[test]
fn test10() {
    let (list, clone) = run_test(10);
    assert_eq!(list, clone);
}

#[test]
fn test100() {
    let (list, clone) = run_test(100);
    assert_eq!(list, clone);
}

#[test]
fn test1000() {
    let (list, clone) = run_test(1000);
    assert_eq!(list, clone);
}

#[test]
fn test10000() {
    let (list, clone) = run_test(10000);
    assert_eq!(list, clone);
}

#[test]
fn test100000() {
    let (list, clone) = run_test(100000);
    assert_eq!(list, clone);
}
