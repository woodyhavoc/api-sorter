# integration.rb

def main
  server = fork do
    exec 'cargo run --bin api-sorter'
  end
  Process.detach(server)

  iterations = ARGV.length == 1 ? ARGV[0].to_i : 10

  iterations.times do |i|
    count = i
    size = i * 10
    system("cargo run --bin client -- -c #{count} -s #{size}")
    raise "Client failed on count: #{count}, size: #{size}" unless $? == 0
  end

end

main
